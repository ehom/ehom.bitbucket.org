// app.js

var controller = {
	"currentOption": 0,

	init() {
		var convert_option_num_to_str_res_root_id = function(optionNumber) {
			// add one to optionNumber since it is zero-based. 
			return 100 * (optionNumber + 1);
		};

		var createEventHandler = function(strElementId) {

			var contents = [];
			var index = parseInt(strElementId.replace("radio-btn-", ""));
			console.log(strElementId);
			console.log("radio button #", index);
			var str_res_id = convert_option_num_to_str_res_root_id(index);
			contents = model.get_strings(str_res_id);

			return function buttonHandler() {
				view.updateContent(contents);
				controller.currentOption = document.getElementById(strElementId).value;
			}
		};

		var createParade = function() {
			var road = [ 
				"&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", 
				"&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", 
				"&#x1F41B;", "&nbsp;", 
				"&#x1F697;", "&nbsp;",
				"&#x1F699;", "&nbsp;", 
				"&#x1F695;", "&nbsp;",
				"&#x1F68C;", "&nbsp;",
				"&#x1F68E;", "&nbsp;",
				"&#x1F43F;", "&nbsp;",
				"&#x1F690;", "&nbsp;",
				"&#x1F422;", "&nbsp;",
				"&#x1F415;", "&nbsp;",
				"&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", 
				"&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", "&nbsp;", 
				];
				return function() {
					road.push(road.shift());
					document.getElementById("contentBoxTitleBar").innerHTML = road.join("");
				}
		};

		model.init();
		var contents = model.get_strings();
		view.init(contents);

		view.getRadioButtons().forEach(function(id) {
			console.log("createEventHandler:", id)
			document.getElementById(id).onclick = createEventHandler(id);
		});

		var interval = setInterval(createParade(), 1000);

		document.getElementById("contentBoxTitleBar").onclick = function() {
			// increment it, and then update the screen with the respective resources
			var previousOption = controller.currentOption;
			var newOption = previousOption + 1;
			console.log("current Option:", previousOption);
			if (newOption > 3) {
				newOption = 0;
			}
			console.log("new Option:", newOption);
			controller.currentOption = newOption;
			var str_res_id = convert_option_num_to_str_res_root_id(newOption);
			var contents = model.get_strings(str_res_id);
			view.update(previousOption, newOption, contents);
		}
	}
};

controller.init();

