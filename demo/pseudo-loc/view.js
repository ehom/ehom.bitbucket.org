// view.js

var view = {
	"radioButtonIds": [ "radio-btn-0", "radio-btn-1", "radio-btn-2", "radio-btn-3" ],

	init(str_array) {
		document.getElementById(this.radioButtonIds[0]).checked = true;

		this.updateContent(str_array);
	},

	getRadioButtons() {
		return this.radioButtonIds;
	},

	update(currentId, newId, str_array) {
		var id = this.radioButtonIds[currentId];
		console.log("view.update: ", id)
		document.getElementById(id).checked = false;			

		id = this.radioButtonIds[newId];
		document.getElementById(id).checked = true;
		this.updateContent(str_array);
	},

	updateContent(str_array) {
		document.getElementById("test-message-1").innerHTML = str_array[0];
		document.getElementById("test-message-2").innerHTML = str_array[1];

		var content = "";
		str_array.slice(2, 13).forEach(function(text, index) {
			content += text;
		});	
		var elementId = "test-message-3";
		document.getElementById(elementId).innerHTML = content;

		document.getElementById("test-message-4").innerHTML = str_array[13];
		document.getElementById("test-message-5").innerHTML = str_array[14];
	}
};
