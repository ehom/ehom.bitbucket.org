// model.js

'use strict';

var model = {
	 "string resources": {
        "100": {
            "1": "Thank You",
            "10": "drive configurations. ",
            "11": "Those who own ",
            "12": "Model S or Model X ",
            "13": "cars will be offered priority production slots as a special thanks for supporting the Tesla mission.",
            "14": "Reservation Confirmation:",
            "15": "Billed to:",
            "2": "Your reservation is confirmed",
            "3": "Model 3 production is scheduled to begin late ",
            "4": "2017",
            "5": ". ",
            "6": "North American ",
            "7": "deliveries will be first, followed by ",
            "8": "Europe, Asia, and Pacific countries with ",
            "9": "right-hand "
        },
        "200": {
            "1": "<span class=\"PseudoLoc brackets\">«</span>Thank You<span class=\"PseudoLoc brackets\">»</span>",
            "10": "<span class=\"PseudoLoc brackets\">«</span>drive configurations. <span class=\"PseudoLoc brackets\">»</span>",
            "11": "<span class=\"PseudoLoc brackets\">«</span>Those who own <span class=\"PseudoLoc brackets\">»</span>",
            "12": "<span class=\"PseudoLoc brackets\">«</span>Model S or Model X <span class=\"PseudoLoc brackets\">»</span>",
            "13": "<span class=\"PseudoLoc brackets\">«</span>cars will be offered priority production slots as a special thanks for supporting the Tesla mission.<span class=\"PseudoLoc brackets\">»</span>",
            "14": "<span class=\"PseudoLoc brackets\">«</span>Reservation Confirmation:<span class=\"PseudoLoc brackets\">»</span>",
            "15": "<span class=\"PseudoLoc brackets\">«</span>Billed to:<span class=\"PseudoLoc brackets\">»</span>",
            "2": "<span class=\"PseudoLoc brackets\">«</span>Your reservation is confirmed<span class=\"PseudoLoc brackets\">»</span>",
            "3": "<span class=\"PseudoLoc brackets\">«</span>Model 3 production is scheduled to begin late <span class=\"PseudoLoc brackets\">»</span>",
            "4": "<span class=\"PseudoLoc brackets\">«</span>2017<span class=\"PseudoLoc brackets\">»</span>",
            "5": "<span class=\"PseudoLoc brackets\">«</span>. <span class=\"PseudoLoc brackets\">»</span>",
            "6": "<span class=\"PseudoLoc brackets\">«</span>North American <span class=\"PseudoLoc brackets\">»</span>",
            "7": "<span class=\"PseudoLoc brackets\">«</span>deliveries will be first, followed by <span class=\"PseudoLoc brackets\">»</span>",
            "8": "<span class=\"PseudoLoc brackets\">«</span>Europe, Asia, and Pacific countries with <span class=\"PseudoLoc brackets\">»</span>",
            "9": "<span class=\"PseudoLoc brackets\">«</span>right-hand <span class=\"PseudoLoc brackets\">»</span>"
        },
        "300": {
            "1": "<span class=\"PseudoLoc brackets\">«</span>Ťḣäṅḳ Ẏöü<span class=\"PseudoLoc brackets\">»</span>",
            "10": "<span class=\"PseudoLoc brackets\">«</span>ḋṙïṿë ċöṅḟïġüṙäṫïöṅṡ. <span class=\"PseudoLoc brackets\">»</span>",
            "11": "<span class=\"PseudoLoc brackets\">«</span>Ťḣöṡë ẇḣö öẇṅ <span class=\"PseudoLoc brackets\">»</span>",
            "12": "<span class=\"PseudoLoc brackets\">«</span>Ṁöḋëḷ Ṡ öṙ Ṁöḋëḷ Ẋ <span class=\"PseudoLoc brackets\">»</span>", 
            "13": "<span class=\"PseudoLoc brackets\">«</span>ċäṙṡ ẇïḷḷ ḃë öḟḟëṙëḋ ṗṙïöṙïṫẏ ṗṙöḋüċṫïöṅ ṡḷöṫṡ äṡ ä ṡṗëċïäḷ ṫḣäṅḳṡ ḟöṙ ṡüṗṗöṙṫïṅġ ṫḣë Ťëṡḷä ṁïṡṡïöṅ.<span class=\"PseudoLoc brackets\">»</span>",
            "14": "<span class=\"PseudoLoc brackets\">«</span>Ṙëṡëṙṿäṫïöṅ Ċöṅḟïṙṁäṫïöṅ:<span class=\"PseudoLoc brackets\">»</span>",
            "15": "<span class=\"PseudoLoc brackets\">«</span>Ḃïḷḷëḋ ṫö:<span class=\"PseudoLoc brackets\">»</span>",
            "2": "<span class=\"PseudoLoc brackets\">«</span>Ẏöüṙ ṙëṡëṙṿäṫïöṅ ïṡ ċöṅḟïṙṁëḋ<span class=\"PseudoLoc brackets\">»</span>",
            "3": "<span class=\"PseudoLoc brackets\">«</span>Ṁöḋëḷ ３ ṗṙöḋüċṫïöṅ ïṡ ṡċḣëḋüḷëḋ ṫö ḃëġïṅ ḷäṫë <span class=\"PseudoLoc brackets\">»</span>",
            "4": "<span class=\"PseudoLoc brackets\">«</span>２０１７<span class=\"PseudoLoc brackets\">»</span>",
            "5": "<span class=\"PseudoLoc brackets\">«</span>. <span class=\"PseudoLoc brackets\">»</span>",
            "6": "<span class=\"PseudoLoc brackets\">«</span>Ṅöṙṫḣ Ȧṁëṙïċäṅ <span class=\"PseudoLoc brackets\">»</span>",
            "7": "<span class=\"PseudoLoc brackets\">«</span>ḋëḷïṿëṙïëṡ ẇïḷḷ ḃë ḟïṙṡṫ, ḟöḷḷöẇëḋ ḃẏ <span class=\"PseudoLoc brackets\">»</span>",
            "8": "<span class=\"PseudoLoc brackets\">«</span>Ëüṙöṗë, Ȧṡïä, äṅḋ Ṗäċïḟïċ ċöüṅṫṙïëṡ ẇïṫḣ <span class=\"PseudoLoc brackets\">»</span>",
            "9": "<span class=\"PseudoLoc brackets\">«</span>ṙïġḣṫ-ḣäṅḋ <span class=\"PseudoLoc brackets\">»</span>"
        },
        "400": {
            "1": "<span class=\"PseudoLoc brackets\">«</span>Ťḣäṅḳ Ẏöü<span class=\"PseudoLoc textExpansion\">☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "10": "<span class=\"PseudoLoc brackets\">«</span>ḋṙïṿë ċöṅḟïġüṙäṫïöṅṡ. <span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "11": "<span class=\"PseudoLoc brackets\">«</span>Ťḣöṡë ẇḣö öẇṅ <span class=\"PseudoLoc textExpansion\">☉☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "12": "<span class=\"PseudoLoc brackets\">«</span>Ṁöḋëḷ Ṡ öṙ Ṁöḋëḷ Ẋ <span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ </span><span class=\"PseudoLoc brackets\">»</span>",
            "13": "<span class=\"PseudoLoc brackets\">«</span>ċäṙṡ ẇïḷḷ ḃë öḟḟëṙëḋ ṗṙïöṙïṫẏ ṗṙöḋüċṫïöṅ ṡḷöṫṡ äṡ ä ṡṗëċïäḷ ṫḣäṅḳṡ ḟöṙ ṡüṗṗöṙṫïṅġ ṫḣë Ťëṡḷä ṁïṡṡïöṅ.<span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ▾▾▾▾▾ ▾▾▾▾▾ ▾▾▾▾▾ ▾▾▾▾▾ ▾▾▾▾▾ </span><span class=\"PseudoLoc brackets\">»</span>",
            "14": "<span class=\"PseudoLoc brackets\">«</span>Ṙëṡëṙṿäṫïöṅ Ċöṅḟïṙṁäṫïöṅ:<span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "15": "<span class=\"PseudoLoc brackets\">«</span>Ḃïḷḷëḋ ṫö:<span class=\"PseudoLoc textExpansion\">☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "2": "<span class=\"PseudoLoc brackets\">«</span>Ẏöüṙ ṙëṡëṙṿäṫïöṅ ïṡ ċöṅḟïṙṁëḋ<span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "3": "<span class=\"PseudoLoc brackets\">«</span>Ṁöḋëḷ ３ ṗṙöḋüċṫïöṅ ïṡ ṡċḣëḋüḷëḋ ṫö ḃëġïṅ ḷäṫë <span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ▾▾▾▾▾ ☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "4": "<span class=\"PseudoLoc brackets\">«</span>２０１７<span class=\"PseudoLoc textExpansion\">☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "5": "<span class=\"PseudoLoc brackets\">«</span>. <span class=\"PseudoLoc brackets\">»</span>",
            "6": "<span class=\"PseudoLoc brackets\">«</span>Ṅöṙṫḣ Ȧṁëṙïċäṅ <span class=\"PseudoLoc textExpansion\">☉☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "7": "<span class=\"PseudoLoc brackets\">«</span>ḋëḷïṿëṙïëṡ ẇïḷḷ ḃë ḟïṙṡṫ, ḟöḷḷöẇëḋ ḃẏ <span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ▾▾▾▾▾ ☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "8": "<span class=\"PseudoLoc brackets\">«</span>Ëüṙöṗë, Ȧṡïä, äṅḋ Ṗäċïḟïċ ċöüṅṫṙïëṡ ẇïṫḣ <span class=\"PseudoLoc textExpansion\">▾▾▾▾▾ ▾▾▾▾▾ ☉☉</span><span class=\"PseudoLoc brackets\">»</span>",
            "9": "<span class=\"PseudoLoc brackets\">«</span>ṙïġḣṫ-ḣäṅḋ <span class=\"PseudoLoc textExpansion\">☉☉☉</span><span class=\"PseudoLoc brackets\">»</span>"
        }
    },

	load_str_resource(id) {
		// console.log("load_str_resource: ", id);
		return this["string resources"][id];
		if (id in this["string resources"]) {
			return this["string resources"][id];
		}
		return "";
	},

	build_string(list_of_ids) {
		var results = [];
		list_of_ids.forEach(function(element){
			results.push(model.load_str_resource(element));
		});
		return results.join("");
	},

	init() {
	},

	get_strings(group_id) {
		if (group_id == undefined) {
			group_id = 100;
		}

		var results = [];
		var stringTable = model['string resources'][group_id.toString()];
		var numStrings = Object.keys(stringTable).length

		console.log("numStrings:", numStrings);

		for (var i = 1; i <= numStrings; i++ ) {
			results.push(stringTable[i.toString()]);
		}
		return results;
	},
};
